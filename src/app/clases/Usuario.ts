export class Usuario{
    id?:number;
    cedula?:number;
    nombres?:string;
    apellidos?:string;
    correo?:string;
    telefono?:string;
}