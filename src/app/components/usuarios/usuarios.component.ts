import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { NgForm, FormControl } from '@angular/forms';

import { Usuario } from 'src/app/clases/Usuario';
import { UsuarioService } from 'src/app/services/usuario.service';
import { NgxSpinnerService } from 'ngx-spinner';
import swal from 'sweetalert2'
import { debug } from 'util';

declare var $: any;
declare var jQuery: any;


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  errores: object[] = [];
  usuario: Usuario = {};
  usuarioVer: Usuario = {};
  usuarios: Usuario[] = [];
  constructor(private usuarioService: UsuarioService , private spinner: NgxSpinnerService) {
    this.spinner.show();
    this.usuarioService.getUsuarios().subscribe(data => {
      this.usuarios = data['usuarios'];
      this.spinner.hide();
    });
  }

  ngOnInit() {
  }
  modalCrearUsuario() {
    this.errores = [];
    this.usuario = {};
    $('#crearUsuarioModal').modal('show');
  }
  guardarUsuario(formulario: NgForm) {
    if (!formulario.valid) {
       return;
    }
    this.spinner.show();
    this.usuarioService.guardarUsuario(this.usuario).subscribe(data => {
      console.log(data);
      if (data.success) {
        if (this.usuario.id == null) {
          this.usuarios.push(data.usuario);
        } else {
          for (let index = 0; index < this.usuarios.length; index++) {
            if (this.usuarios[index].id == this.usuario.id) {
              this.usuarios[index] = data.usuario;
            }
          }
        }

        swal.fire({
            title: "Realizado",
            text: "Acción realizada satisfactoriamente.",
            type: "success",
            timer: 2000,
            showConfirmButton: false
        });

        setTimeout(() => {
          $('#crearUsuarioModal').modal('hide');
        }, 2000);
        formulario.resetForm();
      } else {
        this.errores = data.errores;
      }

    // tslint:disable-next-line:no-unused-expression
    }), error => swal.fire({
      type: 'error',
      title: 'Error',
      text: 'No se pudo efectuar la operación. Intente más tarde.',
    });

    this.spinner.hide();
  }
  editarUsuario(usuario){
    const copy = Object.assign({}, usuario);
    this.errores = [];
    this.usuario = copy;
    $('#crearUsuarioModal').modal('show');
  }
  verUsuario(usuario){
    const copy = Object.assign({}, usuario);
    this.errores = [];
    this.usuarioVer = copy;
    $('#verUsuarioModal').modal('show');
  }
}
