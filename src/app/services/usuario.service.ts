import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  headers = new HttpHeaders({
    'Content-Type': 'aplication/json'
    
  });


  constructor(private httpClient: HttpClient) { }

  getUsuarios(): Observable<any>{
    return this.httpClient.get('http://localhost:8000/usuarios',{headers:this.headers})
    }
    guardarUsuario(usuario): Observable<any>{
      if(usuario.id == null){
        return this.httpClient.post('http://localhost:8000/saveUsuario',usuario,{headers:this.headers})
      }else{
        return this.httpClient.post('http://localhost:8000/updateUsuario',usuario,{headers:this.headers})
      }
    
    }


}