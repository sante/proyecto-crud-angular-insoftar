import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { UsuariosComponent } from './components/usuarios/usuarios.component';



const app_routes: Routes = [
    { path: 'usuarios', component: UsuariosComponent },
    { path: '**', pathMatch: 'full', redirectTo:'usuarios' },
];
export const APP_ROUTING = RouterModule.forRoot(app_routes);